from utility.config import reciept, participants, item_list

def main():
    trigger = False
    while trigger is False:
        scan_reciept()
        enter_participants()
        item_copy()
        for key,value in participants.items():
            enter_portion(value)
        print(item_list)
        trigger = resolve_issues()
    calculate_totals()


def scan_reciept(): 
    pass


def enter_participants():
    pass


def item_copy():
    """
        Copies receipt items to empty item dict.
    """
    global item_list
    item_list = dict(reciept['tx'])
    for key,value in item_list.items():
        value["portion"] = {}


def enter_portion(user):
    """
        Use this to enter in portions of dishes eaten.

        user: key value pairs of user information.
    """
    trigger = False
    
    while trigger is False:
        selection = int(input("Hey {}, what dish are you choosing?".format(user["first_name"])))
        if selection == 0:
            trigger = True
        elif selection > 0 and selection <= len(item_list):
            amount = float(input("How much of the {} did you eat?".format(item_list[selection]["name"])))
            if amount > 0 and amount <= 1:
                #Adds portion to participant list.
                user["dishes"][selection] = amount
                
                #Adds portion to item list.
                user_id = user["id"]
                item_list[selection]["portion"][user_id] = user["dishes"][selection]
            else:
                print("Enter a value between .1 and 1.0")
        else:
            print("Improper selection. Please enter an amount between 0 and {}".format(len(item_list)))
    
    add_tip(user)
        

def add_tip(user):
    """
        as
    """
    trigger = False
    while trigger is False:
        tip = float(input("How much would you like to tip? (Enter an amount between .10 and .30"))
        if tip >= .1 and tip <= .3:
            user["tip"] = tip
            trigger = True
        else:
            print("Enter a correct tip value.")


def resolve_issues():
    #Iterating through each item in the reciept list.
    for key, value in item_list.items():
        total_ratio = 0
        #Iterating through each user that is sharing the item.
        for k,v in value["portion"].items():
            total_ratio += v

        #Readjusting the portions.
        diff = 1.0 - total_ratio
        
        if diff != 0:
            for k,v in value["portion"].items():
                v += (diff * v / total_ratio)
                v = round(v, 4)
                
                user = participants[k]
                user["dishes"][key] = v
                print(user)
                print("{} was adjusted to {}".format(user["first_name"],v))
    return True


def audit_reciept():
    sale = 0
    for key,value in reciept["tx"].items():
        sale += value["amount"]
    
    sale = round(sale,2)
    tax_value = reciept["tax"]["rate"]
    tax = sale * tax_value
    tax = round(tax,2)
    total = sale + tax

def calculate_totals():
    for key, value in participants.items():
        participant_total = 0
        for k,v in value["dishes"].items():
            dish_amount = item_list[k]["amount"] * v
            dish_amount = round(dish_amount,2)
            participant_total += dish_amount
        value["sale"] = round(participant_total, 2)
        value["tax"] = round(reciept["tax"]["rate"] * value["sale"], 2)
        value["total"] = round(value["sale"] + value["tax"], 2)
        value["tip"] = round(reciept["tip"]["rate"] * value["total"], 2)
        value["grand_total"] = round(value["tip"] + value["total"], 2)
        print("{} has to pay Sale: ${} Tax: ${} Tip: ${} Grand Total: ${}".format(
            value["first_name"],value["sale"],value["tax"],value["tip"],value["grand_total"]))

main()