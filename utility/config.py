reciept = {
    "tx": {
        1:{"name": "Flying Dutchman", "amount": 2.40},
        2:{"name": "Double Double", "amount": 4.50},
        3:{"name": "Cheeseburger", "amount": 3.30},
        4:{"name": "French Fries", "amount": 2.70},
        5:{"name": "Milkshake", "amount": 2.90},
        6:{"name": "Soft Drink", "amount": 1.75}
    },
    "sale": 17.55,

    "tax": {
        "rate": .0925,
        "amount": 1.62
    },
    "total": 19.17,

    "tip": {
        "rate": .15,
        "amount": 2.88
    },
    "grand_total": 22.05
}

item_list = {}

participants = {
    1:{
    "id":1,
    "first_name": "Sly",
    "last_name": "Duda",
    "username": "slyduda",
    "venmo_handle": "slyduda",
    "dishes": {
    },
    "tip": .18},
    2:{
    "id":2,
    "first_name": "Steph",
    "last_name": "Duda",
    "username": "stephlamzin",
    "venmo_handle": "st33ph",
    "dishes": {
    },
    "tip": .16},
    3:{
    "id":3,
    "first_name": "Andes",
    "last_name": "U",
    "username": "andesrwu",
    "venmo_handle": "andesu",
    "dishes": {
    },
    "tip": .15}
}
