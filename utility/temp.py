participants = {
    1:{
    "id":1,
    "first_name": "Sly",
    "last_name": "Duda",
    "username": "slyduda",
    "venmo_handle": "slyduda",
    "dishes": [
        {"dish_id":1, "portion": 1.0},
        {"dish_id":4, "portion": .5}
    ],
    "tip": .18},
    2:{
    "id":2,
    "first_name": "Steph",
    "last_name": "Duda",
    "username": "stephlamzin",
    "venmo_handle": "st33ph",
    "dishes": [
        {"dish_id":3, "portion": 1.0},
        {"dish_id":4, "portion": .5},
        {"dish_id":5, "portion": .8}
    ],
    "tip": .16},
    3:{
    "id":3,
    "first_name": "Andes",
    "last_name": "U",
    "username": "andesrwu",
    "venmo_handle": "andesu",
    "dishes": [
        {"dish_id":2, "portion": 1.0},
        {"dish_id":5, "portion": .1},
        {"dish_id":6, "portion": 1.0}
    ],
    "tip": .15}
}
