import hashlib as hl
import json

def hash_string_265(string):
	return hl.sha256(string).hexdigest()

def hash_reciept(reciept):
	hashable_reciept = reciept # Important to keep copy at the end so previous block does not get overwritten.
	return hash_string_265(json.dumps(hashable_reciept, sort_keys=True).encode())